import csv

class CsvParser:
	def __init__(self):
		self.dataList = []

	def readFile(self, fileName):
		with open(fileName) as csvFile:
			csvReader = csv.reader(csvFile, delimiter=',')
			lineCnt = 0
			for row in csvReader:
				if lineCnt != 0:
					rowStr = row[0]+","+row[1]+","+row[2]+","+row[3]+","+row[4]+","+row[5]
					self.dataList.append(rowStr)
				lineCnt += 1

	def getList(self):
		return self.dataList