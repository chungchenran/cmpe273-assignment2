from flask import Flask, request
from flask_restful import Resource, Api
import re, sys

app = Flask(__name__)
api = Api(app)

entries = []
portNum = 5000

if len(sys.argv) > 1:
	portNum = int(sys.argv[1])

class CauseOfDeathList(Resource):
    def get(self):
        return {'num_entries': len(entries), 'entries': entries}

    def post(self):
        entry = {}
        for key, value in request.json.items():
            entry = {key: value}
            entries.append(entry)
        return "201 Created", 201

api.add_resource(CauseOfDeathList, '/api/v1/entries')

if __name__ == '__main__':
	app.run(port=portNum, debug=True)
