from csv_parser import CsvParser
import re, sys, requests

csvparser1 = CsvParser()
servers = ['http://localhost:5000','http://localhost:5001','http://localhost:5002','http://localhost:5003']

class RendezvousHash:
	def __init__(self, fileName):
		csvparser1.readFile(fileName)
		self.dataList = csvparser1.getList()

	def shardData(self):
		for i in range(len(self.dataList)):
			m = re.search('^(.+?),.+?,(.+?),(.+?),.+?,.+?$', self.dataList[i])
			uniqueKey = hash(m.group(1)+":"+m.group(2)+":"+m.group(3))
			entry = {uniqueKey: self.dataList[i]}
			weightList = []
			for j in range(len(servers)):
				weightList.append(hash((self.dataList[i],servers[j])))
			largestWeight = max(weightList)
			serverNum = weightList.index(largestWeight)
			url = servers[serverNum]+"/api/v1/entries"
			headers = {'content-type': 'application/json'}
			r = requests.post(url, json=entry, headers=headers)
		print("Uploaded all %d entries." % len(self.dataList))

	def verifyData(self):
		print("Verifying the data.")
		r0 = requests.get(servers[0]+"/api/v1/entries")
		r1 = requests.get(servers[1]+"/api/v1/entries")
		r2 = requests.get(servers[2]+"/api/v1/entries")
		r3 = requests.get(servers[3]+"/api/v1/entries")
		print("GET %s" % servers[0])
		print(r0.text)
		print("GET %s" % servers[1])
		print(r1.text)
		print("GET %s" % servers[2])
		print(r2.text)
		print("GET %s" % servers[3])
		print(r3.text)

if __name__ == '__main__':
	hrw1 = RendezvousHash(sys.argv[1])
	hrw1.shardData()
	hrw1.verifyData()
